
export const signIn = ( _profile, _role) => {
    sessionStorage.setItem("profile", _profile);
    sessionStorage.setItem("role", _role);
    return {status: true}
}

export const signUp = () => {
        
}

export const signOut = () => {
    sessionStorage.removeItem('profile');
    sessionStorage.removeItem('role');
}

export const getUser = () => {
    return {
        profile: sessionStorage.getItem('profile'), 
        role: sessionStorage.getItem('role')
    };
}

