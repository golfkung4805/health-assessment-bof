import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { getUser } from '../lib/user.js';
import MainMenu from "../components/layout/main";
import { useAppContext } from '../context/state';

const PageIndex = () => {

  const router = useRouter();
  let _useAppContext = useAppContext();

  useEffect(()=>{
  
    const user = getUser();
    if(user['profile'] === null && user['role'] === null) {
      router.push("/login");
    } else {
      router.push("/dashboard");
    }

  },[])

  return (<div></div>);
}

export default PageIndex;
