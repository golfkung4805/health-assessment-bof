import { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { getUser } from '../../lib/user.js';
import MainMenu from "../../components/layout/main";
import { 
  Table,
  Space,
  Spin,
  Button,
  Form,
  Input,
  Typography,
  InputNumber,
  Row,
  Col,
  message,
  Modal,
  Upload,
  Select
} from 'antd';
import { numberCommas } from '../../lib'
import packageType from '../../services/packageType';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';

const { Title } = Typography;

function EmployeeList(props){

  const router = useRouter();
  const [spin, setSpine] = useState(false);
  const [startValue,setStartValue] = useState(null);
  const [endValue,setEndValue] = useState(null);
  const [endOpen,setEndOpen] = useState(false);
  const [dataTable, setDataTable] = useState({data:[]});
  const [department, setDepartment] = useState({data:[]});
  const [disabled, setDisabled] = useState(true);
  const [tableLoading, setTableLoading] = useState(false);
  const [fileUpload, setFileUpload] = useState(false);

  const columns = [
    {
        title: 'ลำดับ',
        dataIndex: 'createdAt',
        align: 'center',
        width: 70,
        render:(text, record, index)=>{
          return <font size="2">{index + 1}</font>;
        }
    },
    {
        title: 'ประเภทผู้บริการ',
        dataIndex: 'service_name',
        align: 'center',
        width: 90,
        render:(text, record, index)=>{
          return <font size="2">{text}</font>;
        }
    },
    {
        title: 'ราบละเอียด',
        dataIndex: 'description',
        align: 'center',
        width: 100,
        render:(text, record, index)=>{
          return <font size="2">{text}</font>;
        }
    },
    {
      title: 'วันที่สร้าง',
      dataIndex: 'createdAt',
      align: 'center',
      width: 70,
      render:(text, record, index)=>{
        return <font size="2">{moment(text).format('DD-MM-YYYY')}</font>;
      }
    },
    {
      title: 'จัดการ',
      dataIndex: 'manage',
      align: 'center',
      width: 90,
      render:(text, record, index)=>{
        return (
          
            (record['activeStatus'] === 'inactive'? <font color="red">ยกเลิก</font> : 
            <Space>
              <Button style={{margin:'0.5rem', fontSize:12}} className="hover-warning" onClick={()=>{router.push(`/package/${record['id']}`)}}>แก้ไข</Button> 
              <Button style={{margin:'0.5rem', fontSize:12}} type="danger" onClick={()=>{handleDelete(record)}} >ลบ</Button>
            </Space>
            )
          
        )
      }
    }
  ];

  useEffect(()=>{
    getPackageType();
  },[]);

  const getPackageType = async () => {
    const resp = await packageType.getAll();
    if(resp.status){
      setDataTable(resp.result)
    }
  }

  const handleDelete = async ({id, pinin, service_name, description}) => {
    let resultConfirm = confirm(`คุณกำลังจะลบข้อมูล package "${service_name}", "${description}" ใช่หรือไม่ ?`);
    if(resultConfirm){

      setSpine(true);

      const response = await packageType.del(id);
      if(response.error){
          message.error("เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง !");
          setSpine(false);
      }else{
          message.success(`ลบข้อมูล package "${service_name}", "${description}" เสร็จสิ้น`);
          getPackageType();
          setSpine(false);
      }

    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
  }

  return (
    <div>
      <Head>
        <title>จัดการ ประเภทผู้บริการ</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row justify="space-around" style={{marginTop:'1rem'}} >
              <Col xs={24}>
                  <Title level={4}>จัดการ ประเภทผู้บริการ</Title>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <div style={{float:'right'}}>
                  <a href="/package/add"><Button style={{float:'right'}} type="primary" shape="round">เพิ่ม ประเภทผู้บริการ</Button></a>
                </div>
              </Col>
            </Row>
            <Row justify="space-around" style={{marginTop:'1rem'}} >
              <Col xs={24}>
                <Table
                  loading={tableLoading}
                  size="small"
                  bordered 
                  columns={columns} 
                  dataSource={dataTable['data'].map((element, index)=>{return Object.assign(element,{key:index});})} 
                  pagination={{ pageSize: 25 }} scroll={{ y: 1440 }}
                />
              </Col>
            </Row>
          </Spin>
        </MainMenu>

      </div>
    </div>
  )
}

export default EmployeeList;