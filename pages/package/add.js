import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect, useState } from 'react';
import { Table } from 'antd';
import { getUser } from '../../lib/user.js';
import MainMenu from "../../components/layout/main";
import packageTypeService from '../../services/packageType';
import { useAppContext } from '../../context/state';
import { 
  Select, 
  Space, 
  Form,
  Button,
  InputNumber,
  DatePicker,
  Input,
  Spin,
  Upload,
  message,
  Modal,
  Row,
  Col
} from 'antd';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';
const { Option } = Select;
const { TextArea } = Input;

const AddWord = (props) => {
  // console.log("props => ",props);
  let _useAppContext = useAppContext();
  const router = useRouter();
  const [form] = Form.useForm();
  const [spin, setSpine] = useState(true);

  useEffect(()=>{
    mainFunc();
  },[])

  const mainFunc = () => {
    const user = getUser();
    if(user['profile'] === null) {
      router.push("/login");
    } else {
      setSpine(false);
    }
  }

  const menuCallback = (param) => {
    console.log("param -> ",param);
    router.push(`/${param}`);
  }

  const onSave = async (value) => {
    setSpine(true);
    const obj = {
        service_name: value.service_name,
        description: value.description
    }
    const response = await packageTypeService.add(obj);
    if(response.status){
      message.success("เพิ่มข้อมูลเสร็จสิ้น");
      router.push("/package");
      setSpine(false);
    }else{
      message.error("กรุณาตรวจสอบข้อมูลให้ถูกต้อง !");
      setSpine(false);
    }
  }

  const handleClickReset = () => {
    let resultConfirm = confirm(`คุณต้องการคืนค่าหรือไม่`);
    if(resultConfirm){
      window.document.getElementById("formAdd").reset();
    }
  }

  return (
    <div>
      <Head>
        <title>Speccode Yiming / เพิ่ม ประเภทผู้บริการ</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row>
              <Col span={24}>
                <h2>เพิ่ม ประเภทผู้บริการ</h2>
              </Col>
            </Row>
            <Row justify="center" style={{ marginTop: "1rem" }}>
              <div className="card-form">
                <Col xs={24}>
                  <Form form={form} name="formAdd" onFinish={onSave} layout={'vertical'} initialValues={{color:'#e6e6e6'}}>

                    <Form.Item label="ชื่อประเภทผู้บริการ" name='service_name' rules={[{ required: true }]} >
                      <Input placeholder="ชื่อประเภทผู้บริการ" style={{ width: "100%" }} />
                    </Form.Item>

                    <Form.Item label="ชื่อประเภทผู้บริการ" name='description' rules={[{ required: true }]} >
                      <TextArea placeholder="ชื่อประเภทผู้บริการ" style={{ width: "100%" }} rows={4} />
                    </Form.Item>

                    <Form.Item >
                      <Row gutter={8} justify="center">
                        <Col xs={0} sm={24}>
                          <center>
                            <Space>
                              <Button id="formAddReset" onClick={()=>{handleClickReset()}}>
                                คืนค่า
                              </Button>

                              <Button
                                id="back"
                                type="dark"
                                onClick={() => {
                                  router.push("/package");
                                }}
                              >
                                กลับไป
                              </Button>

                              <Button type="primary" htmlType="submit">
                                บันทึก
                              </Button>
                            </Space>
                          </center>
                        </Col>

                        <Col xs={24} sm={0}>
                          <Button
                            style={{ margin: "0.2rem 0rem" }}
                            id="addWordReset"
                            htmlType="Reset"
                            block
                          >
                            คืนค่า
                          </Button>

                          <Button
                            style={{ margin: "0.2rem 0rem" }}
                            id="back"
                            type="dark"
                            onClick={() => {
                              router.push("/word");
                            }}
                            block
                          >
                            กลับไป
                          </Button>

                          <Button
                            style={{ margin: "0.2rem 0rem" }}
                            type="primary"
                            htmlType="submit"
                            block
                          >
                            บันทึก
                          </Button>
                        </Col>
                      </Row>
                    </Form.Item>

                  </Form>
                </Col>
              </div>
            </Row>
          </Spin>
        </MainMenu>
      </div>
    </div>
  )
}

export default AddWord;