import Head from 'next/head';
import  { useEffect } from 'react';
import { useRouter } from 'next/router';
import LoginForm from '../components/formSignin';
import { signIn, getUser } from '../lib/user';

const Login = (props) => {

    const router = useRouter();

    useEffect(()=>{
        mainFunc();
    },[]);
    
    const mainFunc = () => {
        const user = getUser();
        if(user['profile'] !== null && user['role'] !== null) {
            router.push("/");
        }
    };

    const loginCallback = async (callback) => {
        try {
            if(callback['data']['username'] === 'super_admin' && callback['data']['password'] === '12345') {
                signIn('super_admin','admin');
                router.push("/");
            } else {
                alert(" username และ password ผิด หรือ ไม่ตรงกัน กรุณาตรวจสอบอีกครั้ง !");
            }

        } catch (error) {
            console.log("error ! ",error);
        }
        
    }

    return (
        <div>
            <Head>
            <title>ระบบประเมินสุขภาพ - เข้าสู่ระบบ</title>
            </Head>
            <div style={{height:"100vh"}}>
                <LoginForm callback={loginCallback} />
            </div>
        </div>
    );
}

export default Login;