import { useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import  { useEffect } from 'react';
import { getUser } from '../lib/user.js';
import MainMenu from "../components/layout/main";
import { 
  Table,
  Space,
  Spin,
  Button,
  Form,
  Input,
  Typography,
  InputNumber,
  Row,
  Col,
  message,
  Modal,
  Upload,
  Select
} from 'antd';
import { numberCommas } from '../lib'
import personnelService from '../services/personnel';
import departmentService from '../services/department';
import moment from 'moment';
import 'moment/locale/th';
import locale from 'antd/lib/locale/th_TH';

const { Title } = Typography;

function EmployeeList(props){

  const router = useRouter();
  const [spin, setSpine] = useState(false);
  const [startValue,setStartValue] = useState(null);
  const [endValue,setEndValue] = useState(null);
  const [endOpen,setEndOpen] = useState(false);
  const [dataTable, setDataTable] = useState({data:[]});
  const [department, setDepartment] = useState({data:[]});
  const [disabled, setDisabled] = useState(true);
  const [tableLoading, setTableLoading] = useState(false);
  const [fileUpload, setFileUpload] = useState(false);



  useEffect(()=>{
    
  },[]);


  const menuCallback = (param) => {
    console.log("param -> ",param);
  }

  return (
    <div>
      <Head>
        <title>ส่วนควบคุม</title>
      </Head>
      <div>
        <MainMenu callback={menuCallback} >
          <Spin  spinning={spin} tip="กำลังโหลด..." >
            <Row justify="space-around" style={{marginTop:'1rem'}} >
              <Col xs={24}>
                  <Title level={4}>ส่วนควบคุม</Title>
              </Col>
            </Row>
            <Row justify="space-around" style={{marginTop:'1rem'}} >
              <Col xs={24}>
                
              </Col>
            </Row>
          </Spin>
        </MainMenu>

      </div>
    </div>
  )
}

export default EmployeeList;