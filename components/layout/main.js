import  React,{ useState, useEffect } from 'react';
import { Layout, Menu, Avatar, Image } from "antd";
import { useRouter } from 'next/router';
import { signOut, getUser } from '../../lib/user.js';
import { useAppContext } from '../../context/state';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    VideoCameraAddOutlined,
    AreaChartOutlined,
    LogoutOutlined,
    ArrowRightOutlined,
    TeamOutlined,
    ProfileOutlined,
    FolderOutlined,
    CalculatorOutlined,
} from "@ant-design/icons";

const { Header, Sider, Content } = Layout;

const MainMenu = (props) => {

    let _useAppContext = useAppContext();
    const router = useRouter();
    const [collapsed, setCollapsed] = useState(false);

    const toggle = () => {
        setCollapsed(!collapsed)
    };

    const logout = () => {
        signOut();
        window.location.href = "/";
    }

    return (
        <Layout id="layout-desktop" style={{minWidth:1024}}>
            <Sider style={{backgroundColor: "#F8F8FF"}} trigger={null} collapsible collapsed={collapsed}>
                <div className="logo">
                    <div style={{padding: '10px 6px 10px 6px', backgroundColor: "#F8F8FF", lineHeight:2}}>
                        {collapsed ?  
                            <center>
                                <img style={{width:42, height:38}} src="/logo.png"/>
                            </center>
                            :
                            <div style={{color: '#000', textAlign: 'center', fontWeight: 500, backgroundColor: '#91CDA8', fontSize: 19, borderRadius: 12}}><a className="mylink" href="/">ระบบประเมินสุขภาพ</a></div>
                        }
                    </div>
                </div>
                    {/* {(typeof window !== 'undefined' && */}
                        <Menu style={{backgroundColor: "#F8F8FF"}} mode="inline">
                            <Menu.Item style={{marginTop: 10, marginBottom: 10, height: 42}} key="dashboard" icon={<AreaChartOutlined />}>
                                <a className="mylink" href="/dashboard">ส่วนควบคุม</a>
                            </Menu.Item>
                            <Menu.Item style={{marginTop: 10, marginBottom: 10, height: 42}} key="about" icon={<ProfileOutlined />}>
                                <a className="mylink" href="/about">เกี่ยวกับเรา</a>
                            </Menu.Item>
                            <Menu.Item style={{marginTop: 10, marginBottom: 10, height: 42}} key="package" icon={<FolderOutlined />}>
                                <a className="mylink" href="/package">ประเภทผู้บริการ</a>
                            </Menu.Item>
                            <Menu.Item style={{marginTop: 10, marginBottom: 10, height: 42}} key="personnel" icon={<TeamOutlined />}>
                                <a className="mylink" href="/personnel">บุคลากร</a>
                            </Menu.Item>
                            <Menu.Item style={{marginTop: 10, marginBottom: 10, height: 42}} key="video" icon={<VideoCameraAddOutlined />}>
                                <a className="mylink" href="/video">วิดิโอ</a>
                            </Menu.Item>
                            <Menu.Item style={{marginTop: 10, marginBottom: 10, height: 42}} key="assessment" icon={<CalculatorOutlined />}>
                                <a className="mylink" href="/assessment">ผลการประเมิน</a>
                            </Menu.Item>
                            <Menu.Item onClick={()=>{logout();}} style={{marginTop: 10, marginBottom: 10, height: 42}} key="logout" icon={<LogoutOutlined />}>
                                ออกจากระบบ
                            </Menu.Item>
                        </Menu>
                    {/* )} */}
            </Sider>
            <Layout className="site-layout">
                <Header className="site-layout-background">
                    <div style={{ paddingTop: '1px'}} >
                        {collapsed ? <MenuUnfoldOutlined  style={{color:'#000'}} onClick={()=>{toggle()}} /> : <MenuFoldOutlined style={{color:'#000'}} onClick={()=>{toggle()}}/> }
                        <strong style={{float:'right', color:'#000'}}>{_useAppContext['user']['data']['username']}</strong>
                    </div>
                </Header>
                <Content style={{ minHeight:"100vh", maxHeight: "100%"}}>
                    <div style={{
                        margin: "28px 20px",
                        padding: 24,
                        minHeight: 400,
                        backgroundColor:"#F8F8FF",
                        borderRadius: 20,
                        boxShadow: "5px 10px 22px #888888"
                    }}>
                        {props.children}
                    </div>
                </Content>
            </Layout>
        </Layout>
    );

}

export default MainMenu;
